# Changelog

All notable changes to this project will be documented in this file.

## [0.1.0] - 11/12/2022

### Added

* Included CV practice

### Modified

* README.md to use it as an index.


## [0.0.0] - 06/12/2022

### File added

* Included both README.md and .gitignore files and create CHANGELOG.md file.
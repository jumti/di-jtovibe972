# Curriculum Vitae - Juan Manuel Tovar Ibáñez
> Viva er beti  

![Image_Kirby](../img/poyo.jpg)
  
Alumno de 2º DAM en **I.E.S. Velázquez**.  
  
Si necesitas ponerte en contacto conmigo puedes [escribirme un correo electrónico](jtovibe972@g.educaand.es).  

***

## Experiencia profesional

* **ASTRONAUTA**(02/2002-02/2015): Viajé al espacio
  + **Nasa**
    - Astronauta.

* **PILOTO MOTO GP**(02/2015-02/2018): Gané todos los campeonatos posibles
  + **Yamaha**
    - Piloto profesional.

* **DUEÑO DE UN HOTEL 5 ESTRELLAS**(02/2018-02/2019): Hotel de 5 estrellas
  + **Rico**
    - Dueño.

* **MCDONALDS**(02/2019-010/2019): Trabajador de Mcdonalds
  + **Mcdonalds**
    - Hacía hamburguesas.

* **ACCESO A GRADO SUPERIOR**(01/2020-06/2020): Prueba Acceso a grado superior
  + **Acceso**
    - Estudio por libre.

* **DAM**(07/2020-Actualidad): Grado Superior DAM
  + **DAM**
    - Alumno.  

***

## Títulos académicos

1. Bachillerato **Tecnológico** / **Prueba de acceso**
2. Cursos en **C#**
3. Inglés *B1*

***

## Habilidades

| Tabla | Habilidades |              Nivel de la habilidad |
|-------|:-----------:|-----------------------------------:|
| 1     |    Dormir   |               :star: :star: :star: |
| 2     | Comer mucho |               :star: :star: :star: |
| 3     |  Dormir más | :star: :star: :star: :star: :star: |
| 4     |     java    |               :star: :star: :star: |

***

## Bloque de código

```java
// Saluda a 6 usuarios
for (int i = 0; i <= 6; i++)
{
    System.out.println("Hola usuario número " + i + "!");
}
```

***

## Tarjeta de visita

Puedes utilizar este JSON para guardar mis datos :blush:  
```json
{
    "name" : "Juan Manuel",
    "surname" : "Tovar Ibáñez",
    "email" : "jtovibe972@g.educaand.es",
    "skills" : 
    [
        {
            "label" : "Dormir",
            "level" : 3
        },
        {
            "label" : "Comer mucho",
            "level" : 3
        },
        {
            "label" : "Dormir más",
            "level" : 5
        },
        {
            "label" : "java",
            "level" : 3
        }
    ]
}
```
Así es como se renderiza un JSON en el navegador dentro de un bloque de código. :wink:
***


